class Window_Base < Window
  # draw_textの制御文字の事前変換するver
  # \cは仕様上最後に設定された色で全て描画
  # \Iは\wとかで勝手に付与されるので削除
  def draw_text_plus(*args)
    text = if args[0].is_a?(Rect)
             1
           else
             4
           end
    oc = Color.new
    oc.set(contents.font.color)
    args[text] = convert_escape_characters(args[text])
    args[text].gsub!(/\eC\[(\d+)\]/i) do
      change_color(text_color(Regexp.last_match(1).to_i))
      ""
    end
    args[text].gsub!(/\eI\[(\d+)\]/i) do
      ""
    end
    draw_text(*args)
    change_color(oc)
  end
end

module NWConst
  module Sw
    ENCOUNT_NONE = 89
  end
end
class Game_Party < Game_Unit
  alias hima_library_update update
  def update
    hima_library_update
    set_library
  end

  def set_library
    all_items.map { |item| $game_library.get_item(item) }
  end
end

class IO
  alias hima_write write
  def write(*a)
    hima_write *a
  rescue SystemCallError => e
  end
end

class Game_Party < Game_Unit
  #--------------------------------------------------------------------------
  # ● 一時メンバーのセット
  #--------------------------------------------------------------------------
  def set_temp_actors(ary)
    ary.uniq!
    @temp_actors = ary[0, party_member_max].select { |member| check_temp_actor(member) }
    $game_player.refresh
    $game_map.need_refresh = true
  end

  def check_temp_actor(actor_id)
    true
  end

  def add_temp_actors(ary)
    @temp_actors += ary.select { |member| check_temp_actor(member) }
    @temp_actors.uniq!
    @temp_actprs = @temp_actors[0, party_member_max]
    $game_player.refresh
    $game_map.need_refresh = true
  end

  def remove_temp_actors(ary)
    @temp_actors.reject! do |temp_actor|
      ary.include?(temp_actor)
    end
    $game_player.refresh
    $game_map.need_refresh = true
  end
end

class Game_Party  < Game_Unit
  def actors
    if temp_actors_use?
      @temp_actors
    else
      @actors
    end
  end

  def exists
    !actors.empty?
  end

  def swap_order(index1, index2)
    actors[index1], actors[index2] = actors[index2], actors[index1]
    $game_player.refresh
  end
end

class Spriteset_Battle
  def update_pictures
    $game_troop.screen.pictures.each do |pic|
      @picture_sprites[pic.number] ||= Sprite_Picture.new(@viewport1, pic)
      @picture_sprites[pic.number].update
    end
  end
end

class Spriteset_Map
  def update_pictures
    $game_map.screen.pictures.each do |pic|
      @picture_sprites[pic.number] ||= Sprite_Picture.new(@viewport1, pic)
      @picture_sprites[pic.number].update
    end
  end
end

class Sprite_Picture < Sprite
  def update_position
    self.x = @picture.x
    self.y = @picture.y
    self.z = 250 + @picture.number
  end
end

class Spriteset_Novel
  def erase_background
    @background.visible = false
    update
  end
end

class Scene_Novel
  def erase_background
    @spriteset.erase_background
  end
end
class Game_Action
  def temptation_targets
    if item.scope == 0
      []
    else
      [$game_actors[NWConst::Actor::LUCA]]
    end
  end
end

class Game_Actor
  def make_auto_battle_actions
    skill_list = skills
    usable_skilltypes = added_skill_types
    skill_list.select! do |skill|
      next if skill.ability?
      next if skill.stypes.all? { |type| !usable_skilltypes.include?(type) }
      next true unless $game_system.conf[:bt_stype]
      !skill.stypes.any? { |type| skill_type_disabled?(type) }
    end
    @actions.size.times do |i|
      @actions[i] = make_auto_battle_action(skill_list)
    end
  end

  def make_auto_battle_action(skill_list)
    action = Game_Action.new(self).set_attack
    until skill_list.empty?
      skill = skill_list.sample
      if opponents_unit.alive_members.size <= 1 && skill.for_all?
        next skill_list.delete(skill)
      end
      next skill_list.delete(skill) if skill.no_auto_battle?
      next skill_list.delete(skill) unless usable?(skill)
      action = Game_Action.new(self).set_skill(skill.id)
      break
    end
    action
  end

  def make_action_list
    list = []
    list.push(Game_Action.new(self).set_attack)
    usable_skills.each do |skill|
      next if skill.no_auto_battle?
      next if opponents_unit.alive_members.size <= 1 && skill.for_all?
      next unless skill.stypes.any? { |type| added_skill_types.include?(type) }

      if $game_system.conf[:bt_stype]
        next if skill.stypes.any? { |type| skill_type_disabled?(type) }
      end
      list.push(Game_Action.new(self).set_skill(skill.id))
    end
    list
  end

end
STDOUT.sync = true


